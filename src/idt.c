/* This file does things with IDTs */

#if !defined(__cplusplus)
#include <stdbool.h>
#endif

#include <stdint.h>
#include <stddef.h>

#define SEL 0x08
#define FLAGS 0x8E
/* Defines an IDT entry */
/*
 *    7                           0 
 *  +---+---+---+---+---+---+---+---+
 *  | P |  DPL  | S |    GateType   |
 *  +---+---+---+---+---+---+---+---+
 * Gatetype is 0b1110, S is 0 for a 32-bit interrupt gate, which is what we need
 * for hardware interrupt.
 * DPL: which ring (0 to 3) (????)
 * P: segment is present? 1=yes
 */
struct idt_entry {
	uint16_t offset_1;
	uint16_t selector;
	uint8_t zero;
	uint8_t type_attr;
	uint16_t offset_2;
} __attribute__((packed)); /* This doesn't add any padding between fields */

struct idt_ptr {
	uint16_t limit;
	uint32_t base;
} __attribute__((packed));

/* Declares an idt_entry pointer (structs are pointers!) of size 256, meaning an
 * IDT of 256 entries */
struct idt_entry idt[256];
struct idt_ptr idtp;

extern void idt_load();

/* Use this function to set an entry in the IDT */
void idt_set_gate(uint8_t num, uint32_t base, uint16_t sel, uint8_t flags) {
	/* Take the argument "base" and split it up into high and low 16-bits,
	 * storing them in idt[num].base_hi and base_lo. The rest of the fields
	 * that you must set in idt[num] are fairly self-explanatory */
	idt[num].selector = sel;
	idt[num].zero = 0;
	idt[num].offset_1 = (base & 0xFFFF);
	idt[num].offset_2 = (base >> 16) & 0xFFFF;
	idt[num].type_attr = flags;
}


void* memset(void *dst, uint32_t c, size_t n) {
	if (n) {
		uint8_t *d = dst;

		do {
			*d++ = c;
		} while (--n);
	}
	return dst;
}

extern void handle_interrupt();
extern void handle_int8();
extern void handle_keyboard();

void idt_install() {
	idtp.limit = (sizeof (struct idt_entry) * 256) -1;
	idtp.base = &idt;

	// zero out IDT
	memset(&idt, 0, sizeof(struct idt_entry) * 256);

	idt_set_gate(0, (unsigned)handle_interrupt, SEL, FLAGS);
	idt_set_gate(1, (unsigned)handle_interrupt, SEL, FLAGS);
	idt_set_gate(2, (unsigned)handle_interrupt, SEL, FLAGS);
	idt_set_gate(3, (unsigned)handle_interrupt, SEL, FLAGS);
	idt_set_gate(4, (unsigned)handle_interrupt, SEL, FLAGS);
	idt_set_gate(5, (unsigned)handle_interrupt, SEL, FLAGS);
	idt_set_gate(6, (unsigned)handle_interrupt, SEL, FLAGS);
	idt_set_gate(7, (unsigned)handle_interrupt, SEL, FLAGS);
	idt_set_gate(8, (unsigned)handle_int8, SEL, FLAGS);
	idt_set_gate(9, (unsigned)handle_interrupt, SEL, FLAGS);
	idt_set_gate(10, (unsigned)handle_interrupt, SEL, FLAGS);
	idt_set_gate(11, (unsigned)handle_interrupt, SEL, FLAGS);
	idt_set_gate(12, (unsigned)handle_interrupt, SEL, FLAGS);
	idt_set_gate(0x21, (unsigned)handle_keyboard, SEL, FLAGS);
	idt_load();
}
