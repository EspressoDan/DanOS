/*
 * pic.c
 *
 *  Created on: Nov 13, 2015
 *      Author: EspressoDan
 */


#include <stddef.h>
#include <stdint.h>
#include "pic.h"

#define MASTER_PIC 0x20
#define SLAVE_PIC 0xA0

#define MASTER_PIC_COMMAND 0x20
#define SLAVE_PIC_COMMAND 0xA0

#define MASTER_PIC_DATA 0x21
#define SLAVE_PIC_DATA 0xA1

/*
 * For PIC remap
 */
#define ICW1_ICW4	0x01		/* ICW4 (not) needed */
#define ICW1_SINGLE	0x02		/* Single (cascade) mode */
#define ICW1_INTERVAL4	0x04		/* Call address interval 4 (8) */
#define ICW1_LEVEL	0x08		/* Level triggered (edge) mode */
#define ICW1_INIT	0x10		/* Initialization - required! */

#define ICW4_8086	0x01		/* 8086/88 (MCS-80/85) mode */
#define ICW4_AUTO	0x02		/* Auto (normal) EOI */
#define ICW4_BUF_SLAVE	0x08		/* Buffered mode/slave */
#define ICW4_BUF_MASTER	0x0C		/* Buffered mode/master */
#define ICW4_SFNM	0x10		/* Special fully nested (not) */


/*
 * Interrupts have to be responded to or they block further interrupts
 * If the IRQ came from the master PIC, they both have to be acknowledged
 */

/* Helper funcs */
static inline void outb(uint16_t port, uint8_t val) {
	asm volatile ( "outb %0, %1"
			: // no output
			: "a"(val), "Nd"(port) );
}

static inline uint8_t inb(uint16_t port) {
    uint8_t ret;
    asm volatile ( "inb %1, %0"
    		: "=a"(ret)
			: "Nd"(port) );
    return ret;
}

static inline void io_wait(void) {
    asm volatile ( "outb %%al, $0x80"
    		:
    		: "a"(0) );
}

void ack_int(uint8_t irq) {
	if (irq >= 8) {
		outb(SLAVE_PIC_COMMAND, 0x20);
	}

	outb(MASTER_PIC_COMMAND, 0x20);
}

/*
arguments:
	offset1 - vector offset for master PIC
		vectors on the master become offset1..offset1+7
	offset2 - same for slave PIC: offset2..offset2+7
*/
void PIC_remap(uint32_t offset1, uint32_t offset2)
{
	uint8_t a1, a2;

	a1 = inb(MASTER_PIC_DATA);                        // save masks
	a2 = inb(SLAVE_PIC_DATA);

	outb(MASTER_PIC_COMMAND, ICW1_INIT+ICW1_ICW4);  // starts the initialization sequence (in cascade mode)
	io_wait();
	outb(SLAVE_PIC_COMMAND, ICW1_INIT+ICW1_ICW4);
	io_wait();
	outb(MASTER_PIC_DATA, offset1);                 // ICW2: Master PIC vector offset
	io_wait();
	outb(SLAVE_PIC_DATA, offset2);                 // ICW2: Slave PIC vector offset
	io_wait();
	outb(MASTER_PIC_DATA, 4);                       // ICW3: tell Master PIC that there is a slave PIC at IRQ2 (0000 0100)
	io_wait();
	outb(SLAVE_PIC_DATA, 2);                       // ICW3: tell Slave PIC its cascade identity (0000 0010)
	io_wait();

	outb(MASTER_PIC_DATA, ICW4_8086);
	io_wait();
	outb(SLAVE_PIC_DATA, ICW4_8086);
	io_wait();

	outb(MASTER_PIC_DATA, a1);   // restore saved masks.
	outb(SLAVE_PIC_DATA, a2);
}

/*
 * PIC has an internal register called the IMR. Its a bitmap of request lines
 * going to the PIC. When the bit is set, the IRQ is ignored by the PIC.
 * These functions set or unset the mask for a given IRQ
 */
void IRQ_set_mask(unsigned char IRQline) {
    uint16_t port;
    uint8_t value;

    if(IRQline < 8) {
        port = MASTER_PIC_DATA;
    } else {
        port = SLAVE_PIC_DATA;
        IRQline -= 8;
    }
    value = inb(port) | (1 << IRQline);
    outb(port, value);
}

void IRQ_clear_mask(unsigned char IRQline) {
    uint16_t port;
    uint8_t value;

    if(IRQline < 8) {
        port = MASTER_PIC_DATA;
    } else {
        port = SLAVE_PIC_DATA;
        IRQline -= 8;
    }
    value = inb(port) & ~(1 << IRQline);
    outb(port, value);
}
