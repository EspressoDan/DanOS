/*
 * scancodes.h
 *
 *  Created on: Nov 14, 2015
 *      Author: EspressoDan
 */

#ifndef SRC_SCANCODES_H_
#define SRC_SCANCODES_H_


#include <stddef.h>
#include <stdint.h>

char read_scancode(unsigned char code);

#endif /* SRC_SCANCODES_H_ */
