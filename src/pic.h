/*
 * pic.h
 *
 *  Created on: Nov 14, 2015
 *      Author: EspressoDan
 */

#ifndef SRC_PIC_H_
#define SRC_PIC_H_

#include <stddef.h>
#include <stdint.h>

void PIC_remap(uint32_t offset1, uint32_t offset2);
void ack_int(uint8_t irq);

#endif /* SRC_PIC_H_ */
