/*
 * scancodes.c
 *
 *  Created on: Nov 14, 2015
 *      Author: EspressoDan
 */

#include "scancodes.h"
#include <stdint.h>
#include <stddef.h>

char read_scancode(unsigned char code) {
	switch (code) {
	case 0x10:
		return 'Q';
	case 0x11:
		return 'W';
	case 0x12:
		return 'E';
	case 0x13:
		return 'R';
	case 0x14:
		return 'T';
	case 0x15:
		return 'Y';
	case 0x16:
		return 'U';
	case 0x17:
		return 'I';
	case 0x18:
		return 'O';
	case 0x19:
		return 'P';
	case 0x1C:
		return '\n';
	case 0x1E:
		return 'A';
	case 0x1F:
		return 'S';
	case 0x20:
		return 'D';
	case 0x21:
		return 'F';
	case 0x22:
		return 'G';
	case 0x23:
		return 'H';
	case 0x24:
		return 'J';
	case 0x25:
		return 'K';
	case 0x26:
		return 'L';
	case 0x27:
		return ';';
	case 0x28:
		return '\'';
	case 0x2C:
		return 'Z';
	case 0x2D:
		return 'X';
	case 0x2E:
		return 'C';
	case 0x2F:
		return 'V';
	case 0x30:
		return 'B';
	case 0x31:
		return 'N';
	case 0x32:
		return 'M';
	case 0x33:
		return ',';
	case 0x34:
		return '.';
	case 0x39:
		return ' ';
	default:
		return '-';
	}
}

