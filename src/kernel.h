/*
 * kernel.h
 *
 *  Created on: Nov 13, 2015
 *      Author: EspressoDan
 */

#ifndef SRC_KERNEL_H_
#define SRC_KERNEL_H_

void terminal_writestring(const char* data);
void terminal_clear();
void terminal_putchar(char c);

#endif /* SRC_KERNEL_H_ */
