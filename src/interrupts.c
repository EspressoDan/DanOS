/*
 * This file contains interrupt service routines for all exceptions generated
 * by the CPU
 */

#include "kernel.h"
#include "scancodes.h"
#include "pic.h"

void int8_handler() {
	terminal_writestring("Double fault received!\n");
	terminal_writestring("An interrupt happened that wasn't on the IDT\n");
	/* Probably should call main or something */
}

void generic_interrupt_handler() {
	terminal_writestring("Interrupt received.\n");
}

static inline void outb(uint16_t port, uint8_t val) {
	asm volatile ( "outb %0, %1"
			: // no output
			: "a"(val), "Nd"(port) );
}

static inline unsigned char inb(uint16_t port) {
	unsigned char ret;
	asm volatile ( "inb %1, %0"
			: "=a"(ret)
			: "Nd"(port) );
	return ret;
}

void keyboard_handler() {
	unsigned char code = inb(0x60);
	char letter = read_scancode(code);
	if (letter != '-') {
		terminal_putchar(letter);
	}
	ack_int(0x1);

}
