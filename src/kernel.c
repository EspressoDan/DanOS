#if !defined(__cplusplus)
#include <stdbool.h> /* C doesn't have booleans by default. */
#endif
#include <stddef.h>
#include <stdint.h>
#include "idt.h"
#include "pic.h"

/* Check if the compiler thinks we are targeting the wrong operating system. */
#if defined(__linux__)
#error "You are not using a cross-compiler, you will most certainly run into trouble"
#endif

/* Hardware text mode color constants. */
enum vga_color {
	COLOR_BLACK = 0,
	COLOR_BLUE = 1,
	COLOR_GREEN = 2,
	COLOR_CYAN = 3,
	COLOR_RED = 4,
	COLOR_MAGENTA = 5,
	COLOR_BROWN = 6,
	COLOR_LIGHT_GREY = 7,
	COLOR_DARK_GREY = 8,
	COLOR_LIGHT_BLUE = 9,
	COLOR_LIGHT_GREEN = 10,
	COLOR_LIGHT_CYAN = 11,
	COLOR_LIGHT_RED = 12,
	COLOR_LIGHT_MAGENTA = 13,
	COLOR_LIGHT_BROWN = 14,
	COLOR_WHITE = 15,
};

/* VGA has two bytes to determine color. 0xAB where A is the background color
 * and b is the foreground color.
 * This functions constructs these two bytes given the passed in fg and bg
 * colors.
 */
uint8_t make_color(enum vga_color fg, enum vga_color bg) {
	return fg | bg << 4;
}

/* Converts a color returned from make_color and an input char into a 16bit
 * element suitable for writing to the VGA memory buffer. */ 
uint16_t make_vgaentry(char c, uint8_t color) {
	uint16_t c16 = c;
	uint16_t color16 = color;
	return c16 | color16 << 8;
}

/* Reimplementation of strlen because <string.h> doesn't exist */
size_t strlen(const char* str) {
	size_t ret = 0;
	while ( str[ret] != 0 )
		ret++;
	return ret;
}

/* Terminals are 80x25. Any other size and you're a heathen.
 * See: http://crawl.develz.org/learndb/#hugeterm
 */
static const size_t VGA_WIDTH = 80;
static const size_t VGA_HEIGHT = 25;

size_t terminal_row;
size_t terminal_column;
uint8_t terminal_color;
uint16_t* terminal_buffer;

/* Writes ' ' aka nothing with a black background to the entire terminal.
 * Clears screen.
 */
void terminal_clear() {
	terminal_row = 0;
	terminal_column = 0;
	terminal_color = make_color(COLOR_LIGHT_GREY, COLOR_BLACK);
	// VGA buffer starts at 0xB8000
	terminal_buffer = (uint16_t*) 0xB8000;
	for (size_t y = 0; y < VGA_HEIGHT; y++) {
		for (size_t x = 0; x < VGA_WIDTH; x++) {
			const size_t index = y * VGA_WIDTH + x;
			terminal_buffer[index] = make_vgaentry(' ', terminal_color);
		}
	}
}

void terminal_setcolor(uint8_t color) {
	terminal_color = color;
}

/* puts a char at the given xy coordinate */
void terminal_putentryat(char c, uint8_t color, size_t x, size_t y) {
	const size_t index = y * VGA_WIDTH + x;
	terminal_buffer[index] = make_vgaentry(c, color);
}

/* Puts the char at the next available spot and increments accordingly */
void terminal_putchar(char c) {
	if (c == '\n') {
		// Essentially just increment terminal_row and reset terminal_column
		if (++terminal_row == VGA_HEIGHT) {
			terminal_row = 0;
		}
		terminal_column = 0;
		return;
	}
	terminal_putentryat(c, terminal_color, terminal_column, terminal_row);

	if (++terminal_column == VGA_WIDTH) {
		terminal_column = 0;
		if (++terminal_row == VGA_HEIGHT) {
			terminal_row = 0;
		}
	}
}

/* Writes the string given to the VGA buffer */
void terminal_writestring(const char* data) {
	size_t datalen = strlen(data);
	for (size_t i = 0; i < datalen; i++)
		terminal_putchar(data[i]);
}

#if defined(__cplusplus)
extern "C" /* Use C linkage for kernel_main. */
#endif

static inline void outb(uint16_t port, uint8_t val) {
	asm volatile ( "outb %0, %1"
			: // no output
			: "a"(val), "Nd"(port) );
}
void kmain() {
	/* Initialize terminal interface */
	terminal_clear();
	/*
	 * init the PIC so IRQ O calls INT 20.
	 * Slave PIC IRQ0 calls INT28
	 */
	PIC_remap(0x20, 0x28);
	outb(0x21,0xfd);
	outb(0xa1,0xff);
	/*
	// Only enable keyboard IRQs from the PIC
	for (int i = 0; i < 15; i++) {
		if (i == 1) {
			IRQ_set_mask(i);
		} else {
			IRQ_clear_mask(i);
		}
	}
	*/
	/* Set up the IDT for interrupts */
	idt_install();
	// turn interrupts on
	asm volatile("sti");
	terminal_writestring("Welcome to DanOS!\n");

	// Ensure that I do not return to assembly
	for (;;) {
		asm volatile("hlt");
	}
}
