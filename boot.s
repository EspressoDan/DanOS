# Declare constants used for creating a multiboot header
.set ALIGN, 	1<<0 # align loaded modules on page boundaries
.set MEMINFO,   1<<1 # provide memory map
.set FLAGS,      ALIGN | MEMINFO # this is the Multiboot 'flag' field
.set MAGIC,     0x1BADB002 # 'magic number' lets bootloader find header
.set CHECKSUM, -(MAGIC + FLAGS) # checksum of above, to prove we are multiboot

# Declare the multiboot header
.section .multiboot
.align 4
.long MAGIC
.long FLAGS
.long CHECKSUM

# Declare space for stack
.section .bootstrap_stack, "aw", @nobits
stack_bottom:
.skip 16384 # 16 KiB
stack_top:

# The linker script specifies _start as the entry point to the kernel and the
# bootloader will jump to this position once the kernel has been loaded. It
# doesn't make sense to return from this function as the bootlaoder is gone.
# Text means code
.section .text

.global _start
.type _start, @function
_start:

	# Set esp (stack pointer) to the top of the stack
	movl $stack_top, %esp

	# Kmain won't return because it endlessly calls HLT
	call kmain

.global handle_interrupt
handle_interrupt:
	cli
	pushal
	cld
	call generic_interrupt_handler
	popal
	sti
	iret

.global handle_int8
handle_int8:
	cli
	pushal
	cld
	call int8_handler
	popal
	hlt

.global handle_keyboard
handle_keyboard:
	pushal
	cld
	call keyboard_handler
	popal
	iret

.global idt_load
.type idt_load, @function
.extern idtp
idt_load:
	lidt idtp
	ret

# Set the size of the _start symbol to the current localtion '.' minus its
# start. This is useful when debugging or when you implement call tracing.
.size _start, . - _start
