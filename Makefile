# List of all non-source files ethat are part of the distribution
AUXFILES := Makefile #README

# All subdirectories holding actual sources. None exist currently, so we
# search the correct directory
PROJDIRS := .

# Finds all files that begin with .c or .h in all the project dirs
SRCFILES := $(shell find . -type f -name "*.c")
HDRFILES := $(shell find . -type f -name "*.h")

# Lists all source files, with c subbed for o
OBJFILES := $(patsubst %.c,%.o,$(SRCFILES))

# Lists all source files with c subbed for _t, test file executables
# Probably won't use this for a while
TSTFILES := $(patsubst %.c,%_t,$(SRCFILES))

# GCC creates a dependency file (that ends in .d) for each source file
# which contains a Makefile dependency rule listing source file's includes
DEPFILES := $(patsubst %.c,%.d,$(SRCFILES))
TSTDEPFILES := $(patsubst %,%.d,$(TSTFILES))

# List of all Files that end up in the distrubution iso
ALLFILES := $(SRCFILES) $(HDRFILES) $(AUXFILES)

# If I have a file named clean in the working dir, make doesn't do anything
# because the file "clean" already exists. Phony checking disables the
# checking of a file of that name
.PHONY: all clean dist todolist

# Shitloads of CFLAGS
WARNINGS := -Wall -Wextra -pedantic -Wshadow -Wpointer-arith -Wcast-align \
	    -Wwrite-strings -Wmissing-prototypes -Wmissing-declarations \
	    -Wredundant-decls -Wnested-externs -Winline -Wno-long-long \
	    -Wuninitialized -Wconversion -Wstrict-prototypes

CFLAGS := -g -std=gnu99 -ffreestanding -O2 $(WARNINGS)

# Define CC as my cross compiler
CC := ~/Documents/School/Projects/crosscomp/opt/cross/bin/i686-elf-gcc -T 

############################################################################
# Makefile Build Rules
# Note that @ silences the Make so no commands are echoed to terminal
# Note that - tells Make to continue in case of an error and not Stop.
############################################################################

all: DanOS.bin
	@$(CC) linker.ld -o DanOS.bin -ffreestanding -O2 -nostdlib $(OBJFILES) boot.o -lgcc

DanOS.bin: boot.o $(OBJFILES) 

boot.o: boot.s
	@~/Documents/School/Projects/crosscomp/opt/cross/bin/i686-elf-as boot.s -o boot.o

# Prints out all files with TODO and FIXME in their file
todolist:
	@for file in $(ALLFILES:Makefile=); do fgrep -H -n -e TODO -e FIXME $$file; done; true

clean:
	@rm $(wildcard boot.o $(OBJFILES) $(DEPFILES) DanOS.bin)

emu: all
	@qemu-system-i386 -kernel DanOS.bin
# Dependency fuckery
# "include" generated dependency files, making them listed in part of the
# Makefile.
# "Never mind that they might not even exist when we run in the first time"
-include $(DEPFILES) # $(TSTDEPFILES)

# -MMD generates a .d file which holds in Makefile syntax rules to make
#  the generated file, in this case a .o
#  Means the object file gets recreated whenever sources are touched
#  If you want to depend on system headers (I might) use -MD
#  -MP adds empty dummy rules, avoiding errors should header files be
#  removed from the filesystem.
%.o: %.c Makefile
	@$(CC) $(CFLAGS) -MMD -MP -c $< -o $@

# %_t: %.c Makefile DanOS.bin
# $(CC) $(CFLAGS) -MMD -MP -DTEST $< DanOS.bin -o $@
